// Package mailinabox enables interactions with a Mail-in-a-box instance
// by sending requests to the provided admin API endpoints.
package mailinabox

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/pkg/errors"
)

// HTTPClient defines the behavior a client must have to be used by this
// package in order to communicate with the Mail-in-a-box API.
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client is the HTTPClient used to make HTTP requests to the API of the
// Mail-in-a-box instance.
var Client HTTPClient = http.DefaultClient

// Mail-in-a-box Admin Paths
const (
	userAdminPath      = "admin/mail/users"
	aliasAdminPath     = "admin/mail/aliases"
	customDNSAdminPath = "admin/dns/custom"
	dnsAdminPath       = "admin/dns"
	sslAdminPath       = "admin/ssl"
)

// Mail-in-a-box Response Messages
const (
	respInvalidCredentials = "Incorrect email address or password."
	respNotAdmin           = "You are not an administrator."
	respInvalidEmail       = "Invalid email address."
	respNoPassword         = "No password provided."
	respNoForward          = "The alias must either forward to an address or have a permitted sender."
	respIPv6InA            = "That's an IPv6 address."
	respIPv4InAAAA         = "That's an IPv4 address."
)

// Errors
var (
	ErrInvalidCredentials = errors.New("incorrect email address or password")
	ErrNotAdmin           = errors.New("you do not have admin privileges")
	ErrUserNotFound       = errors.New("no user found with the given email")
	ErrInvalidEmail       = errors.New("invalid email provided")
	ErrNoPassword         = errors.New("no password provided for new user")
	ErrNoForward          = errors.New("alias needs a forward to address or permitted sender")
	ErrAliasNotFound      = errors.New("no alias found with the given address")
	ErrNoRecordValue      = errors.New("no value provided for DNS record")
	ErrIPv6InARecord      = errors.New("cannot set an IPv6 address for A record")
	ErrIPv4InAAAARecord   = errors.New("cannot set an IPv4 address for AAAA record")
)

// Config contains the information needed to send authenticated HTTP requests
// to Mail-in-a-box.
//
// Host should be the full address to your Mail-in-a-box instance
// Email and Password should be correct credentials for an existing user on
// your Mail-in-a-box instance with admin privileges.
type Config struct {
	Host     string
	Email    string
	Password string
}

// Users

// DomainUsers represents all the existing users for a particular domain in
// the Mail-in-a-box instance.
type DomainUsers struct {
	Domain string `json:"domain"`
	Users  []User `json:"users"`
}

// User represents an existing user in the Mail-in-a-box instance.
type User struct {
	Email      string   `json:"email"`
	Privileges []string `json:"privileges"`
	Status     string   `json:"status"`
}

// IsActive indicates if the user is an active account in the Mail-in-a-box
// instance.
func (u User) IsActive() bool {
	return u.Status == "active"
}

// IsAdmin determines if the user has admin privileges for the Mail-in-a-box
// instance.
func (u User) IsAdmin() bool {
	for _, privilege := range u.Privileges {
		if privilege == "admin" {
			return true
		}
	}

	return false
}

// NewUser is the information needed to create a new user in a
// Mail-in-a-box instance.
type NewUser struct {
	Email    string
	Password string
}

// UsersByDomain returns all existing users registered for a Mail-in-a-box instance.
// The users will be grouped by the domain they are registered to.
func UsersByDomain(config Config) ([]DomainUsers, error) {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return nil, errors.Wrap(err, "parsing host url")
	}

	u.Path = userAdminPath
	q := u.Query()
	q.Add("format", "json")
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return nil, errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return nil, ErrInvalidCredentials
		case respNotAdmin:
			return nil, ErrNotAdmin
		default:
			return nil, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	var du []DomainUsers
	err = json.Unmarshal(body, &du)
	if err != nil {
		return nil, errors.Wrap(err, "parsing JSON response")
	}

	return du, nil
}

// Users returns every active user registered to a Mail-in-a-box instance.
func Users(config Config) ([]User, error) {
	du, err := UsersByDomain(config)
	if err != nil {
		return nil, errors.Wrap(err, "retrieving users")
	}

	var users []User
	for _, d := range du {
		users = append(users, d.Users...)
	}

	return users, nil
}

// AdminUsers returns a slice of all users that currently have admin privilege.
func AdminUsers(config Config) ([]User, error) {
	du, err := UsersByDomain(config)
	if err != nil {
		return nil, errors.Wrap(err, "retrieving users")
	}

	var admins []User
	for _, d := range du {
		for _, u := range d.Users {
			if u.IsAdmin() {
				admins = append(admins, u)
			}
		}
	}

	return admins, nil
}

// AddUser registers a new user in a Mail-in-a-box instance using the
// credentials provided.
func AddUser(config Config, nu NewUser) error {
	form := url.Values{}
	form.Set("email", nu.Email)
	form.Set("password", nu.Password)

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = userAdminPath + "/add"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusBadRequest {
		switch string(body) {
		case respInvalidEmail:
			return ErrInvalidEmail
		case respNoPassword:
			return ErrNoPassword
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// RemoveUser archives the user with the given email so it cannot be used anymore.
func RemoveUser(config Config, email string) error {
	form := url.Values{}
	form.Set("email", email)

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return err
	}
	u.Path = userAdminPath + "/remove"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return err
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusBadRequest {
		return ErrUserNotFound
	}

	if resp.StatusCode == http.StatusUnauthorized {
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Wrap(err, "reading response body")
		}

		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// AddAdminPrivilege gives the user with the given email admin privileges.
func AddAdminPrivilege(config Config, email string) error {
	form := url.Values{}
	form.Set("email", email)
	form.Set("privilege", "admin")

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return err
	}
	u.Path = userAdminPath + "/privileges/add"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return err
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusBadRequest {
		return ErrUserNotFound
	}

	if resp.StatusCode == http.StatusUnauthorized {
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Wrap(err, "reading response body")
		}

		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// RemoveAdminPrivilege removes the admin privilege from the user with the
// given email.
func RemoveAdminPrivilege(config Config, email string) error {
	form := url.Values{}
	form.Set("email", email)
	form.Set("privilege", "admin")

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return err
	}
	u.Path = userAdminPath + "/privileges/remove"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return err
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusBadRequest {
		return ErrUserNotFound
	}

	if resp.StatusCode == http.StatusUnauthorized {
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Wrap(err, "reading response body")
		}

		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// SetPassword updates the password of the user with the given email to the
// provided password.
func SetPassword(config Config, email, password string) error {
	form := url.Values{}
	form.Set("email", email)
	form.Set("password", password)

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return err
	}
	u.Path = userAdminPath + "/password"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return err
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusBadRequest {
		switch string(body) {
		case respNoPassword:
			return ErrNoPassword
		default:
			return ErrUserNotFound
		}
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// Aliases

// Alias is an existing address alias being used by a Mail-in-a-box instance
// to redirect incoming emails to other addresses.
type Alias struct {
	Address          string   `json:"address"`
	AddressDisplay   string   `json:"address_display"`
	Auto             bool     `json:"auto"`
	ForwardsTo       []string `json:"forwards_to"`
	PermittedSenders []string `json:"permitted_senders"`
}

// NewAlias is the information needed to create a new alias.
type NewAlias struct {
	Address          string
	ForwardsTo       []string
	PermittedSenders []string
}

// DomainAliases represents all the aliases associated with a particular domain.
type DomainAliases struct {
	Domain  string  `json:"domain"`
	Aliases []Alias `json:"aliases"`
}

// AliasesByDomain shows all existing aliases for a Mail-in-a-box instance.
// The aliases will be grouped by the domain they are registered to.
func AliasesByDomain(config Config) ([]DomainAliases, error) {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return nil, errors.Wrap(err, "parsing host url")
	}

	u.Path = aliasAdminPath
	q := u.Query()
	q.Add("format", "json")
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return nil, errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return nil, ErrInvalidCredentials
		case respNotAdmin:
			return nil, ErrNotAdmin
		default:
			return nil, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	var da []DomainAliases
	err = json.Unmarshal(body, &da)
	if err != nil {
		return nil, errors.Wrap(err, "parsing JSON response")
	}

	return da, nil
}

// Aliases shows all existing aliases for a Mail-in-a-box instance.
func Aliases(config Config) ([]Alias, error) {
	da, err := AliasesByDomain(config)
	if err != nil {
		return nil, errors.Wrap(err, "retrieving aliases")
	}

	var aliases []Alias
	for _, d := range da {
		aliases = append(aliases, d.Aliases...)
	}

	return aliases, nil
}

// AddAlias registers a new alias to a Mail-in-a-box instance.
// If the NewAlias argument has no values in either ForwardsTo
// or PermittedSenders, this will fail.
func AddAlias(config Config, na NewAlias) error {
	form := url.Values{}
	form.Set("address", na.Address)
	for _, ft := range na.ForwardsTo {
		form.Add("forwards_to", ft)
	}

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = aliasAdminPath + "/add"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusBadRequest {
		switch string(body) {
		case respInvalidEmail:
			return ErrInvalidEmail
		case respNoForward:
			return ErrNoForward
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// RemoveAlias will remove a alias currently set up for a Mail-in-a-box instance.
func RemoveAlias(config Config, address string) error {
	form := url.Values{}
	form.Set("address", address)

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = aliasAdminPath + "/remove"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusBadRequest {
		return ErrAliasNotFound
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// Custom DNS

// DNS Record RTypes
const (
	DNSRecordTypeA     string = "A"
	DNSRecordTypeAAAA  string = "AAAA"
	DNSRecordTypeCAA   string = "CAA"
	DNSRecordTypeCNAME string = "CNAME"
	DNSRecordTypeTXT   string = "TXT"
	DNSRecordTypeMX    string = "MX"
	DNSRecordTypeSRV   string = "SRV"
	DNSRecordTypeSSHFP string = "SSHFP"
	DNSRecordTypeNS    string = "NS"
)

// CustomDNSRecord represents a DNS record that was manually added by a user.
type CustomDNSRecord struct {
	QName string `json:"qname"`
	RType string `json:"rtype"`
	Value string `json:"value"`
}

// CustomDNSRecords retrieves any existing DNS Records from a Mail-in-a-box instance
// that match the given query name and record type.
//
// If qname and rtype are both empty strings, ALL records will be returned.
// If only qname is a non empty string, will return all "A" records that match
// that query name. ex: "www.donaldfeury.xyz"
func CustomDNSRecords(config Config, qname, rtype string) ([]CustomDNSRecord, error) {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return nil, errors.Wrap(err, "parsing host url")
	}

	u.Path = customDNSAdminPath

	if qname != "" {
		u.Path += "/" + qname
	}

	if qname != "" && rtype != "" {
		u.Path += "/" + rtype
	}

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "creating GET request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return nil, errors.Wrap(err, "sending GET request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return nil, ErrInvalidCredentials
		case respNotAdmin:
			return nil, ErrNotAdmin
		default:
			return nil, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	var dr []CustomDNSRecord
	err = json.Unmarshal(body, &dr)
	if err != nil {
		return nil, errors.Wrap(err, "parsing JSON response")
	}

	return dr, nil
}

// AddCustomDNSRecord creates a new DNS record with the given query name, record type,
// and value.
//
// AddCustomDNSRecord will create a new record, even if one with the same query
// name and record type already exists.
//
// If value is an empty string, the request will fail.
// If rtype is an empty string, the record type will default to "A".
func AddCustomDNSRecord(config Config, qname, rtype, value string) error {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = fmt.Sprintf("%s/%s", customDNSAdminPath, qname)

	if rtype != "" {
		u.Path += "/" + rtype
	}

	var data io.Reader

	if value != "" {
		data = strings.NewReader(value)
	}

	req, err := http.NewRequest(http.MethodPost, u.String(), data)
	if err != nil {
		return errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	if data != nil {
		req.Header.Set("Content-Type", "text/plain")
	}

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusBadRequest {
		switch string(body) {
		case respIPv4InAAAA:
			return ErrIPv4InAAAARecord
		case respIPv6InA:
			return ErrIPv6InARecord
		default:
			return ErrNoRecordValue
		}
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// UpdateCustomDNSRecord updates the value of an existing custom DNS record
// that matches the given query name and record type with the given value.
//
// UpdateCustomDNSRecord will only ever create or update one record.
//
// If rtype is an empty string, it defaults to a "A" record type.
func UpdateCustomDNSRecord(config Config, qname, rtype, value string) error {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = fmt.Sprintf("%s/%s", customDNSAdminPath, qname)

	if rtype != "" {
		u.Path += "/" + rtype
	}

	var data io.Reader

	if value != "" {
		data = strings.NewReader(value)
	}

	req, err := http.NewRequest(http.MethodPut, u.String(), data)
	if err != nil {
		return errors.Wrap(err, "creating PUT request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	if data != nil {
		req.Header.Set("Content-Type", "text/plain")
	}

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending PUT request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusBadRequest {
		switch string(body) {
		case respIPv4InAAAA:
			return ErrIPv4InAAAARecord
		case respIPv6InA:
			return ErrIPv6InARecord
		default:
			return ErrNoRecordValue
		}
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// RemoveCustomDNSRecord removes an existing custom DNS record by matching up
// on the given query name, record type, and value.
//
// If rtype is an empty string, it defaults to a "A" record type.
// If value is an empty string, ALL records matching the given query name and
// record type will be removed.
func RemoveCustomDNSRecord(config Config, qname, rtype, value string) error {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = fmt.Sprintf("%s/%s", customDNSAdminPath, qname)

	if rtype != "" {
		u.Path += "/" + rtype
	}

	var data io.Reader

	if value != "" {
		data = strings.NewReader(value)
	}

	req, err := http.NewRequest(http.MethodDelete, u.String(), data)
	if err != nil {
		return errors.Wrap(err, "creating DELETE request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	if data != nil {
		req.Header.Set("Content-Type", "text/plain")
	}

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending DELETE request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("reading response body: %w", err)
	}

	if resp.StatusCode == http.StatusBadRequest {
		return ErrNoRecordValue
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// DNS

// DNSRecord is a single entry of a DNS record returned from the non-custom
// DNS endpoints.
//
// Main difference between this and CustomDNSRecord is the presence of a field
// explaning why the record exists.
type DNSRecord struct {
	Explanation string `json:"explanation"`
	QName       string `json:"qname"`
	RType       string `json:"rtype"`
	Value       string `json:"value"`
}

// DomainDump contains a domain managed by the Mail-in-a-box instance and all
// DNS records associated with it.
//
// This is part of the payload returned from DumpDNS
type DomainDump struct {
	Domain  string      `json:"domain"`
	Records []DNSRecord `json:"records"`
}

// DNSDump contains all the domains managed by the Mail-in-a-box instance as
// well as all the records associated with each domain.
type DNSDump struct {
	DomainDumps []DomainDump `json:"domainDump"`
}

// UnmarshalJSON overrides the behavior when unmarshaling the dns dump response
// because the payload returned from that endpoint is a fucking mess.
func (dump *DNSDump) UnmarshalJSON(data []byte) error {
	var v [][]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	for _, drs := range v {
		var domainRecord DomainDump
		domain, _ := drs[0].(string)
		domainRecord.Domain = domain

		records, _ := drs[1].([]interface{})
		for _, r := range records {
			values, _ := r.(map[string]interface{})
			explanation, _ := values["explanation"].(string)
			qname, _ := values["qname"].(string)
			rtype, _ := values["rtype"].(string)
			value, _ := values["value"].(string)
			record := DNSRecord{explanation, qname, rtype, value}
			domainRecord.Records = append(domainRecord.Records, record)
		}
		dump.DomainDumps = append(dump.DomainDumps, domainRecord)
	}
	return nil
}

// DumpDNS returns ALL DNS records (including non-custom entries) grouped by the
// domain the entries are associated with.
func DumpDNS(config Config) (DNSDump, error) {
	var dump DNSDump
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return dump, errors.Wrap(err, "parsing host url")
	}

	u.Path = dnsAdminPath + "/dump"

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return dump, errors.Wrap(err, "creating GET request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return dump, errors.Wrap(err, "sending GET request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return dump, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return dump, ErrInvalidCredentials
		case respNotAdmin:
			return dump, ErrNotAdmin
		default:
			return dump, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	err = json.Unmarshal(body, &dump)
	if err != nil {
		return dump, errors.Wrap(err, "parsing JSON response")
	}

	return dump, nil
}

// DNSZones returns a slice of all top level domains managed by the
// Mail-in-a-box instance.
func DNSZones(config Config) ([]string, error) {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return nil, errors.Wrap(err, "parsing host url")
	}

	u.Path = dnsAdminPath + "/zones"

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "creating GET request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return nil, errors.Wrap(err, "sending GET request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return nil, ErrInvalidCredentials
		case respNotAdmin:
			return nil, ErrNotAdmin
		default:
			return nil, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	var zones []string
	err = json.Unmarshal(body, &zones)
	if err != nil {
		return nil, errors.Wrap(err, "parsing JSON response")
	}

	return zones, nil
}

// DNSZoneFile retrieves the zone file for the given zone.
func DNSZoneFile(config Config, zone string) ([]byte, error) {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return nil, errors.Wrap(err, "parsing host url")
	}

	u.Path = fmt.Sprintf("%s/%s/%s", dnsAdminPath, "zonefile", zone)

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "creating GET request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return nil, errors.Wrap(err, "sending GET request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return nil, ErrInvalidCredentials
		case respNotAdmin:
			return nil, ErrNotAdmin
		default:
			return nil, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return body, nil
}

// TriggerDNSUpdate triggers the DNS on a Mail-in-a-box to create new zone files
// and restart 'nsd'.
//
// If force is true, the process will happen even if no changes have been
// detected.
func TriggerDNSUpdate(config Config, force bool) error {
	form := url.Values{}
	form.Set("force", "0")

	if force {
		form.Set("force", "1")
	}

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = dnsAdminPath + "/update"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// SecondaryNameservers retrieves a list of all secondary nameserver hostnames
// that are set up on the Mail-in-a-box instance.
func SecondaryNameservers(config Config) ([]string, error) {
	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return nil, errors.Wrap(err, "parsing host url")
	}

	u.Path = dnsAdminPath + "/secondary-nameserver"

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "creating GET request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return nil, errors.Wrap(err, "sending GET request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return nil, ErrInvalidCredentials
		case respNotAdmin:
			return nil, ErrNotAdmin
		default:
			return nil, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	var response struct {
		Hostnames []string `json:"hostnames"`
	}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, errors.Wrap(err, "parsing JSON response")
	}

	return response.Hostnames, nil
}

// AddSecondaryNameservers adds additional secondary nameservers to a
// Mail-in-a-box instance.
func AddSecondaryNameservers(config Config, hostnames []string) error {
	form := url.Values{}
	form.Set("hostnames", strings.Join(hostnames, ","))

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return errors.Wrap(err, "parsing host url")
	}
	u.Path = dnsAdminPath + "/secondary-nameserver"

	req, err := http.NewRequest(
		http.MethodPost,
		u.String(),
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return errors.Wrap(err, "creating POST request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "sending POST request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return ErrInvalidCredentials
		case respNotAdmin:
			return ErrNotAdmin
		default:
			return fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	return nil
}

// SSL

// SSLStatus represents the current SSL status of a specific domain or
// subdomain.
type SSLStatus struct {
	Domain string `json:"domain"`
	Status string `json:"status"`
	Text   string `json:"text"`
}

// SSLReport indicates which domains can be provisioned and the SSL status of
// each domain managed by the Mail-in-a-box instance.
type SSLReport struct {
	CanProvision []string    `json:"can_provision"`
	Status       []SSLStatus `json:"status"`
}

// CheckSSLStatus returns a full SSL report of the domains managed by the
// Mail-in-a-box instance.
func CheckSSLStatus(config Config) (SSLReport, error) {
	var report SSLReport

	u, err := url.ParseRequestURI(config.Host)
	if err != nil {
		return report, errors.Wrap(err, "parsing host url")
	}

	u.Path = sslAdminPath + "/status"

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return report, errors.Wrap(err, "creating GET request")
	}

	req.SetBasicAuth(config.Email, config.Password)
	resp, err := Client.Do(req)

	if err != nil {
		return report, errors.Wrap(err, "sending GET request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return report, errors.Wrap(err, "reading response body")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		switch string(body) {
		case respInvalidCredentials:
			return report, ErrInvalidCredentials
		case respNotAdmin:
			return report, ErrNotAdmin
		default:
			return report, fmt.Errorf("error response from Mail-in-a-box: %s", string(body))
		}
	}

	err = json.Unmarshal(body, &report)
	if err != nil {
		return report, errors.Wrap(err, "parsing JSON response")
	}

	return report, nil
}
