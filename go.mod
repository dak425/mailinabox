module gitlab.com/dak425/mailinabox

go 1.17

require (
	github.com/ardanlabs/conf/v2 v2.2.0
	github.com/pkg/errors v0.9.1
)
