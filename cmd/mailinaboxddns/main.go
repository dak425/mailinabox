package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/ardanlabs/conf/v2"
	"gitlab.com/dak425/mailinabox"
)

var (
	build  = "develop"
	commit = "unknown"
)

const (
	ipv4URL string = "https://v4.ident.me"
	ipv6URL string = "https://v6.ident.me"
	prefix  string = "MAILINABOX_DDNS"
)

func main() {
	log := log.New(os.Stdout, prefix+": ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	if err := run(log); err != nil {
		log.Fatal(err)
	}
}

func run(log *log.Logger) error {
	config := struct {
		conf.Version
		Host     string        `conf:"required,help:URL to your Mail-in-a-box instance"`
		Email    string        `conf:"required,help:Email address of a user with admin privilege"`
		Password string        `conf:"required,mask,help:Password of the user with admin privilege"`
		Interval time.Duration `conf:"default:30m,help:Duration to wait between updating DNS records,short:i"`
		Domains  []string      `conf:"required,help:List of domains to add the A and AAAA records separated by semicolons"`
		IPv4     bool          `conf:"default:true,help:Set A records to your IPv4 address,env:IPv4,flag:IPv4"`
		IPv6     bool          `conf:"default:true,help:Set AAAA records to your IPv6 address,env:IPv6,flag:IPv6"`
	}{
		Version: conf.Version{
			Build: fmt.Sprintf("%s - %s", build, commit),
			Desc:  "Dynamic DNS solution powered by your Mail-in-a-box instance",
		},
	}

	help, err := conf.Parse(prefix, &config)
	if err != nil {
		if errors.Is(err, conf.ErrHelpWanted) {
			fmt.Println(help)
			return nil
		}
		return fmt.Errorf("parsing config: %w", err)
	}

	if !config.IPv4 && !config.IPv6 {
		return errors.New("both IPv4 and IPv6 are disabled, no work to do")
	}

	out, err := conf.String(&config)
	if err != nil {
		return fmt.Errorf("generating config for output: %w", err)
	}

	log.Println(out)

	// Setup Mail-in-a-box config
	mailinaboxcfg := mailinabox.Config{
		Host:     config.Host,
		Email:    config.Email,
		Password: config.Password,
	}

	// How often should the records be updated
	interval := config.Interval

	// Which domains should records be updated for
	domains := config.Domains

	// Start ticker in goroutine
	ticker := time.NewTicker(interval)
	done := make(chan bool)

	log.Println("starting Mail-in-a-box DDNS...")
	defer log.Println("shutdown complete")
	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				// Get your IPv4 and IPv6 addresses from ident.me
				addresses, err := getIPAddresses()
				if err != nil {
					log.Printf("could not determine IP addresses")
					continue
				}
				for _, d := range domains {
					if config.IPv4 {
						// Update IPv4 DNS A records
						log.Printf("updating DNS A record for '%s'...\n", d)
						if err := mailinabox.UpdateCustomDNSRecord(mailinaboxcfg, d, "A", addresses[0]); err != nil {
							log.Printf("could not update DNS record: %s\n", err)
						}
					}

					if config.IPv6 {
						// Update IPv6 DNS AAAA records
						log.Printf("updating DNS AAAA record for '%s'...\n", d)
						if err := mailinabox.UpdateCustomDNSRecord(mailinaboxcfg, d, "AAAA", addresses[1]); err != nil {
							log.Printf("could not update DNS record: %s\n", err)
						}
					}
				}
			}
		}
	}()

	// Create signal chat to get shutdown signals from OS
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	log.Println("DDNS now running")

	// Wait for shutdown signal from OS
	<-shutdown

	log.Println("shutting down Mail-in-a-box DDNS...")

	// Stop ticker and send signal to stop goroutine
	ticker.Stop()
	done <- true

	return nil
}

func getIPAddresses() ([]string, error) {
	resp, err := http.Get(ipv4URL)
	if err != nil {
		return nil, err
	}

	ipv4, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	resp.Body.Close()

	resp, err = http.Get(ipv6URL)
	if err != nil {
		return nil, err
	}

	ipv6, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	resp.Body.Close()

	return []string{string(ipv4), string(ipv6)}, nil
}
