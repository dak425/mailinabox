PACKAGE_VERSION=0.1.0
DDNS_VERSION=0.1.0
COMMIT_SHA=$(shell git rev-parse --short HEAD)
BIN_DIR=bin

all: clean create-bin-dir build-ddns

build-ddns:
	go build -o ${BIN_DIR}/mailinaboxddns -ldflags '-X main.version=$(DDNS_VERSION) -X main.commit=$(COMMIT_SHA)' cmd/mailinaboxddns/main.go

clean:
	[ -d ${BIN_DIR} ] && rm -rf ${BIN_DIR} || true

create-bin-dir:
	mkdir -p ${BIN_DIR}

.PHONY: clean build-ddns all