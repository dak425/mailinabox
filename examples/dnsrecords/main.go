package main

import (
	"fmt"
	"os"

	"gitlab.com/dak425/mailinabox"
)

func main() {
	config := mailinabox.Config{
		Host:     os.Getenv("MAILINABOX_HOST"),
		Email:    os.Getenv("MAILINABOX_EMAIL"),
		Password: os.Getenv("MAILINABOX_PASSWORD"),
	}

	// Get ALL customRecords
	customRecords, err := mailinabox.CustomDNSRecords(config, "", "")

	if err != nil {
		fmt.Printf("could not remove admin privilege: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("DNS Records: %+v\n", customRecords)

	// Get All "A" records that match "www.donaldfeury.xyz"
	qname := "www.donaldfeury.xyz"
	customRecords, err = mailinabox.CustomDNSRecords(config, qname, "")
	if err != nil {
		fmt.Printf("could not remove admin privilege: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("DNS Records: %+v\n", customRecords)

	// Get All "TXT" records that match "donaldfeury.xyz"
	qname = "donaldfeury.xyz"
	customRecords, err = mailinabox.CustomDNSRecords(config, qname, mailinabox.DNSRecordTypeTXT)
	if err != nil {
		fmt.Printf("could not remove admin privilege: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("DNS Records: %+v\n", customRecords)

	// Get a dump off ALL DNS records, including the non-custom records
	records, err := mailinabox.DumpDNS(config)
	if err != nil {
		fmt.Printf("could not dump dns records: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("DNS Records: %+v\n", records)
}
