package main

import (
	"fmt"
	"os"

	"gitlab.com/dak425/mailinabox"
)

func main() {
	config := mailinabox.Config{
		Host:     os.Getenv("MAILINABOX_HOST"),
		Email:    os.Getenv("MAILINABOX_EMAIL"),
		Password: os.Getenv("MAILINABOX_PASSWORD"),
	}
	email := "test@donaldfeury.xyz"

	err := mailinabox.RemoveUser(config, email)
	if err != nil {
		fmt.Printf("could not register new user: %s\n", err)
		os.Exit(1)
	}
}
