package main

import (
	"fmt"
	"os"

	"gitlab.com/dak425/mailinabox"
)

func main() {
	config := mailinabox.Config{
		Host:     os.Getenv("MAILINABOX_HOST"),
		Email:    os.Getenv("MAILINABOX_EMAIL"),
		Password: os.Getenv("MAILINABOX_PASSWORD"),
	}

	users, err := mailinabox.Users(config)
	if err != nil {
		fmt.Printf("could not retrieve users: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Users: %+v", users)
}
