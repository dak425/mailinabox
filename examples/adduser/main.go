package main

import (
	"fmt"
	"os"

	"gitlab.com/dak425/mailinabox"
)

func main() {
	config := mailinabox.Config{
		Host:     os.Getenv("MAILINABOX_HOST"),
		Email:    os.Getenv("MAILINABOX_EMAIL"),
		Password: os.Getenv("MAILINABOX_PASSWORD"),
	}

	user := mailinabox.NewUser{
		Email:    "test@donaldfeury.xyz",
		Password: "a_very_strong_secure_password",
	}

	err := mailinabox.AddUser(config, user)
	if err != nil {
		fmt.Printf("could not register new user: %s\n", err)
		os.Exit(1)
	}
}
